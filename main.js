var express = require('express');
var bodyParser = require('body-parser');
var database = 	require('./elasticsearchClient');
var uuid = require('node-uuid');
var result = require('./result');
var api = require('./routes/api');



/**
 *
 * Server application
 *
 */
var Server = function() {
	
	var self = this;
	
	
	/**
	 *  Configuration of modules and variables
	 */
	self.configModulesAndVariables = function(){
		
		self.express = express();
		self.express.use(express.static('page'));
		self.express.use(bodyParser.urlencoded({ extended: true }));
        self.urlencodedParser = bodyParser.urlencoded({ extended: false });
		self.uuid = uuid;
		self.result = result.create();
		self.result.initialize();
		self.db = database.create(self);
		self.db.initialize();
		self.SERVER_PORT = process.env.OPENSHIFT_NODEJS_PORT || 8081;
		self.SERVER_ADDRESS = process.env.OPENSHIFT_NODEJS_IP || "localhost";
		self.api = api.create(self);
	};
	
	
	
	
	/**
	 * Routes creation
	 */
	self.createRoutes = function() {

		self.routes = {};
		self.routes['/api/getRoomFromCode']				= self.api.getRoomFromCode;
		self.routes['/api/bindQRCode']					= self.api.bindQRCode;
		self.routes['/api/setCurrentRoom']				= self.api.setCurrentRoom;
		self.routes['/api/deleteCurrentRoom']			= self.api.deleteCurrentRoom;
		self.routes['/api/getCurrentRoom']				= self.api.getCurrentRoom;
	
	};
	
	
	
	
    /**
     *  Initialize the server (express)
     *  Create the routes and register the handlers.
     */
    self.initializeServer = function() {

		self.express.get('/', function (req, res) {
			res.sendFile( __dirname + "/page/index.php" );
		})


		self.createRoutes();
		for (var r in self.routes) { 
            self.express.post(r, self.routes[r]);
        }
		
		self.routes['/api/getAvailableProfessors']		= self.api.getAvailableProfessors;
		self.express.get('/api/getAvailableProfessors',self.routes['/api/getAvailableProfessors']);
	};
	
	/**
     *  Initialization
     */
    self.initialize = function() {
        self.configModulesAndVariables();
        self.initializeServer();
    };

	/**
	 *  Start the server
	 */
	self.start = function() {
        self.express.listen(self.SERVER_PORT, self.SERVER_ADDRESS, function() {
            console.log('Node server started on %s:%d', self.SERVER_ADDRESS, self.SERVER_PORT);
        });
    };
};

/**
 * main()
 */
var server = new Server();
server.initialize();
server.createRoutes();
server.start();


/*
app.get('/', function (req, res) {
   res.sendFile( __dirname + "/" + "index.htm" );
})

app.get('/put_test_element', function (req, res) {
   
})

app.get('/get_test_element', function (req, res) {
   
})

*/

         


/*
app.get('/process_get', function (req, res) {

   // Prepare output in JSON format
   response = {
       first_name:req.query.first_name,
       last_name:req.query.last_name,
	   provaModule:myModuleInstance.hello()
   };
   console.log(response);
   res.end(JSON.stringify(response));
})

app.post('/process_post', urlencodedParser, function (req, res) {

   // Prepare output in JSON format
   response = {
       first_name:req.body.first_name,
       last_name:req.body.last_name
   };
   console.log(response);
   res.end(JSON.stringify(response));
})
*/
