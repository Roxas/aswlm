var elasticsearch = require('elasticsearch');

module.exports = {

    create: function (app) {
		
		var self = this;

		/**
         *  Initialization
         */
		self.initialize = function() {
			// Connect the client to two nodes, requests will be
			// load-balanced between them using round-robin
			self.client = elasticsearch.Client({
			  host: 'aswappelasticsearch-uniboapps.rhcloud.com' /*|| 'localhost:9200'*/
			}); 
			self.app = app;
		};



		self.insert = function() {
			//.index
			
		};
		self.get = function(data, callback) {
			self.client.get(data, function (error, response) {
				
				if(error){
					console.log(error);
					callback(1, response);
				}else{
					callback(0, response);
				}

			});
		};

		self.search = function(data, callback) {
			self.client.search(data, function (error, response) {
				if(error){
					console.log(error);
					callback(3, response);
				}else{
					callback(0, response);
				}

			});
		};

        /**
		 * Update
		 *
		 * @param data
		 * @param callback
         */
		self.update = function(data,callback){
			
			self.client.update(data, function (error, response) {
				if(error){
					console.log(error);
					callback(2, response);
				}else{
					callback(0, response);
				}

			});
			
		};

		self.delete = function() {
			//.delete
		};
		
		
		
		
		return self;
	}
};


